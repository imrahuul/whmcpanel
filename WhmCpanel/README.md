## cPanel/WHM API PHP

Manage your WHM/cPanel server with this PHP library. Simple to use. With this PHP library, you can manage your cPanel/WHM server.


### Installation

You can install this library with composer

- add below code to ```composer.json``` of your project
  ```
    "repositories": [
        {
            "type": "path",
            "url": "<folder where you kept the package>/WhmCpanel",
            "options": {
                    "symlink": true
                }
            }
    ]
```

```bash
composer require unify/cpanel-whm @dev
```

Note - If Memory limit issue came in bash then , try

```bash
COMPOSER_MEMORY_LIMIT=-1 composer require unify/cpanel-whm @dev
```


### Usage
Just add :-  use Unify\CpanelWhm\WhmCpanel;

```php
<?php

use Unify\CpanelWhm\WhmCpanel; 

require "vendor/autoload.php";

$WhmCpanel = new WhmCpanel("whmUserName","whmPassword", "whmHost");
$WhmCpanel->createNewAccount($param);

```


### Available Functionality
- WHM
  - Accounts
    - createNewAccount (Create a new account)
    - setupAddonDomain (Add domain on an existing account)
    - suspendAccount (suspend an existing account)
    - addDomainRedirect (adds a redirect to a domain)


