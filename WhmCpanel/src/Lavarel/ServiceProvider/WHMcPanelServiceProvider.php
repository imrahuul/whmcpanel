<?php

namespace Unify\CpanelWhm\Laravel\ServiceProvider;

use Unify\CpanelWhm\WhmCpanel;
use Illuminate\Support\ServiceProvider;

class WHMcPanelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Unify\CpanelWhm\WhmCpanel', function ($app) {
          return new WhmCpanel();
        });
    }
    
}
