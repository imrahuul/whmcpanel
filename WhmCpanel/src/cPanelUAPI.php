<?php

namespace Unify\CpanelWhm;

use Exception;


/**
 * Class cPanelAPI
 */
class cPanelUAPI
{
	public $version = '1.1';
	public $scope = ""; //String - Module we want to use
	public $ssl = 1; //Bool - TRUE / FALSE for ssl connection
	public $port = 2083; //default for ssl servers.
	public $server;
	public $maxredirect = 0; //Number of redirects to make, typically 0 is fine. on some shared setups this will need to be increased.
	protected $api;
	protected $auth;
	protected $user;
	protected $pass;
	protected $secret;
	protected $type;
	protected $session;
	protected $method;
	protected $requestUrl;

	/**
	 * @param $user
	 * @param $pass
	 * @param $server
	 */
	function __construct($server, $user, $pass)
	{
		$this->user = $user;
		$this->pass = $pass;
		$this->server = $server;
		$this->setApi('uapi');
	}

	protected function setApi($api)
	{
		$this->api = $api;
		$this->setMethod();
	}

	public function setScope($scope)
    {
        $this->scope = $scope;
    }

	protected function setMethod()
	{
		switch ($this->api)
		{
			case 'uapi':
				$this->method = '/execute/';
				break;
			case 'api2':
				$this->method = '/json-api/cpanel/';
				break;
			default:
				throw new Exception('$this->api is not set or is incorrectly set. The only available options are \'uapi\' or \'api2\'');
		}
	}

	public function __call($name, $arguments)
	{
		if (count($arguments) < 1 || !is_array($arguments[0]))
			$arguments[0] = array();
		return json_decode($this->APIcall($name, $arguments[0]));
	}

	protected function APIcall($name, $arguments)
	{
		$this->auth = base64_encode($this->user . ":" . $this->pass);
		$this->type = $this->ssl == 1 ? "https://" : "http://";
		$this->requestUrl = $this->type . $this->server . ':' . $this->port . $this->method;
		if ($this->scope != '')
		{
				$this->requestUrl .= ($this->scope != '' ? $this->scope . "/" : '') . $name . '?';
		}
		else
		{
			throw new Exception('Scope must be set.');
		}
		foreach ($arguments as $key => $value)
		{
			$this->requestUrl .= $key . "=" . urlencode($value) . "&";
		}
		return $this->curl_request($this->requestUrl);
	}

	/**
	 * @param $url
	 * @return bool|mixed
	 */
	protected function curl_request($url)
	{

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Basic " . $this->auth));
		curl_setopt($ch, CURLOPT_TIMEOUT, 100020);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$content = $this->curl_exec_follow($ch, $this->maxredirect);
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);

		curl_close($ch);

		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['content'] = $content;
		return $header['content'];
	}

	/**
	 * @param $ch
	 * @param null $maxredirect
	 * @return bool|mixed
	 */
	protected function curl_exec_follow($ch, &$maxredirect = null)
	{

		// we emulate a browser here since some websites detect
		// us as a bot and don't let us do our job
		$user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5)" .
			" Gecko/20041107 Firefox/1.0";
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

		$mr = $maxredirect === null ? 5 : intval($maxredirect);

		if (ini_get('open_basedir') == '' && ini_get('safe_mode') == 'Off')
		{

			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $mr > 0);
			curl_setopt($ch, CURLOPT_MAXREDIRS, $mr);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		}
		return curl_exec($ch);
	}

	public function fetchLastRequest()
    {
        return $this->requestUrl;
    }
}

