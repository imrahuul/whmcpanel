<?php

namespace Unify\CpanelWhm;

use Exception;
use Unify\CpanelWhm\WhmCpanelApi;
use Unify\CpanelWhm\cPanelUAPI;
use Unify\CpanelWhm\Exceptions\InvalidDataException;
use Unify\CpanelWhm\Exceptions\EmptyInstanceException;

class WhmCpanel {

		protected $apiInstance,
		$whmHost,
		$whmUserName,
		$whmPassword;

	public function __construct($paramArray = null) {
			// pr($paramArray,0);
			$this->whmUserName = isset($paramArray['whmUserName']) ? $paramArray['whmUserName'] : '';
			$this->whmPassword = isset($paramArray['whmPassword']) ? $paramArray['whmPassword'] : '';
			$this->whmHost = isset($paramArray['whmHost']) ? $paramArray['whmHost'] : '';
			$this->apiInstance = new cPanelUAPI($this->whmHost, $this->whmUserName, $this->whmPassword);

	}

	public function set_apiInstance($api)
    {
        if ($api == 'whm') {
            $this->apiInstance = new WhmCpanelApi($this->whmHost, $this->whmUserName, $this->whmPassword);
        }
    }	

    /**
    * Create a cPanel Account
    *
    * This function will allow one to create an account, the $acctconf parameter requires that the follow
    * three associations are defined:
    *	- username
    *	- password
    *	- domain
    *
    * Failure to prive these will cause an error to be logged.  Any other key/value pairs as defined by the createaccount call
    * documentation are allowed parameters for this call.
    *
    * @param array $acctconf
    * @return mixed
    * @link https://documentation.cpanel.net/display/DD/WHM+API+1+Functions+-+createacct XML API Call documentation
    */
	public function createNewAccount($accountDetail = [])
	{
		$this->set_apiInstance('whm');
		if (!empty($accountDetail) && $this->apiInstance)
		{
			$details = ['username' => isset($accountDetail['username']) ? $accountDetail['username'] : '',
				'domain'           => isset($accountDetail['domain']) ? $accountDetail['domain'] : '',
				'plan'             => isset($accountDetail['plan']) ? $accountDetail['plan'] : '',
				'pkgname'          => isset($accountDetail['pkgname']) ? $accountDetail['pkgname'] : 0,
				'savepkg'          => isset($accountDetail['savepkg']) ? $accountDetail['savepkg'] : 0,
				'featurelist'      => isset($accountDetail['featurelist']) ? $accountDetail['featurelist'] : '',
				'quota'            => isset($accountDetail['quota']) ? $accountDetail['quota'] : 0,
				'password'         => isset($accountDetail['password']) ? $accountDetail['password'] : '',
				'ip'               => isset($accountDetail['ip']) ? $accountDetail['ip'] : 'n',
				'cgi'              => isset($accountDetail['cgi']) ? $accountDetail['cgi'] : 1,
				'frontpage'        => isset($accountDetail['frontpage']) ? $accountDetail['frontpage'] : 0,
				'hasshell'         => isset($accountDetail['hasshell']) ? $accountDetail['hasshell'] : 1,
				'contactemail'	   => isset($accountDetail['contactemail']) ? $accountDetail['contactemail'] : '',
				'maxftp'		   => isset($accountDetail['maxftp']) ? $accountDetail['maxftp'] : 'unlimited',
				'maxsql'		   => isset($accountDetail['maxsql']) ? $accountDetail['maxsql'] : 'unlimited',
				'maxpop'		   => isset($accountDetail['maxpop']) ? $accountDetail['maxpop'] : 'unlimited',
				'maxlst'		   => isset($accountDetail['maxlst']) ? $accountDetail['maxlst'] : 'unlimited',
				'maxsub'		   => isset($accountDetail['maxsub']) ? $accountDetail['maxsub'] : 'unlimited',
				'maxpark' 		   => isset($accountDetail['maxpark']) ? $accountDetail['maxpark'] : 'unlimited',
				'maxaddon'		   => isset($accountDetail['maxaddon']) ? $accountDetail['maxaddon'] : 'unlimited',
				'bwlimit'          => isset($accountDetail['bwlimit']) ? $accountDetail['bwlimit'] : 'unlimited',
				'customip'         => isset($accountDetail['customip']) ? $accountDetail['customip'] : '',
				'language'         => isset($accountDetail['language']) ? $accountDetail['language'] : 'en',
				'useregns'         => isset($accountDetail['useregns']) ? $accountDetail['useregns'] : 0,
				'hasuseregns'      => isset($accountDetail['hasuseregns']) ? $accountDetail['hasuseregns'] : '',
				'reseller'         => isset($accountDetail['reseller']) ? $accountDetail['reseller'] : 0,
				'forcedns'         => isset($accountDetail['forcedns']) ? $accountDetail['forcedns'] : 0,
				'mxcheck'          => isset($accountDetail['mxcheck']) ? $accountDetail['mxcheck'] : 'auto',
				'MAX_EMAIL_PER_HOUR' => isset($accountDetail['MAX_EMAIL_PER_HOUR']) ? $accountDetail['MAX_EMAIL_PER_HOUR'] : 'unlimited',
				'MAX_DEFER_FAIL_PERCENTAGE' => isset($accountDetail['MAX_DEFER_FAIL_PERCENTAGE']) ? $accountDetail['MAX_DEFER_FAIL_PERCENTAGE'] : 'unlimited',
				'uid'             => isset($accountDetail['uid']) ? $accountDetail['uid'] : '',
				'gid'             => isset($accountDetail['gid']) ? $accountDetail['gid'] : '',
				'homedir'         => isset($accountDetail['homedir']) ? $accountDetail['homedir'] : '',
				'dkim'            => isset($accountDetail['dkim']) ? $accountDetail['dkim'] : 1,
				'spf'             => isset($accountDetail['spf']) ? $accountDetail['spf'] : 1,
				'owner'           => isset($accountDetail['owner']) ? $accountDetail['owner'] : 'root'
			];
			return $this->apiInstance->createacct($details);
			}
			return false;			
	}

	public function getTestOutput()
	{
		$this->set_apiInstance('whm');
		return $this->apiInstance->get_testresult();
	}

	/**
	 * This function return the last request made to this package
	*/
	public function getLastRequest()
	{
		// $this->set_apiInstance('whm');
		if ($this->apiInstance) {
			return $this->apiInstance->fetchLastRequest();
		}
	}


	/**
	 * Add new addonDomain on existing WHM account 
	 * @param string [cPanel_user_name] 
	 * @param Array [add_on_detail] 
	 * @param string [add_on_detail[dir]] 
	 * @param string [add_on_detail[newdomain]]
	 * @param string [add_on_detail[subdomain]]
	 * @return response 
	 * @link https://documentation.cpanel.net/display/DD/cPanel+API+2+Functions+-+AddonDomain%3A%3Aaddaddondomain
	 */
	public function setupAddonDomain($param = [])
	{
		$this->set_apiInstance('whm');
		if (!empty($param))
		{
			$userName = isset($param['cPanel_user_name']) ? $param['cPanel_user_name'] : '';
			$addOnDetail = isset($param['add_on_detail']) ? $param['add_on_detail'] : '';
			if (!empty($userName) && !empty($addOnDetail))
			{
				// $this->apiInstance->setDebug(1);
				return $this->apiInstance->cpanelAPI2Query($userName, 'AddonDomain', 'addaddondomain', $addOnDetail);
			}
		}
		return false;
	}

	/**
	 * Add new addonDomain on existing WHM account 
	 * @param string [cPanel_user_name] 
	 * @param Array [listParam] 
	 * @param string [listParam[regex]], A Perl Compatible Regular Expression that filters the results by the domain key.
	 * Note : Always use regex to filter results , as huge amount of data returns
	 * @param string [add_on_detail[return_https_redirect_status]] to get any redirect status
	 * @return response 
	 * @link https://documentation.cpanel.net/display/DD/cPanel+API+2+Functions+-+AddonDomain%3A%3Alistaddondomains
	 */
	public function listAddonDomains($param = [])
	{
		$this->set_apiInstance('whm');
		if (!empty($param))
		{
			$userName = isset($param['cPanel_user_name']) ? $param['cPanel_user_name'] : '';
			$listParam = isset($param['listParam']) ? $param['listParam'] : '';
			if (!empty($userName))
			{
				// $this->apiInstance->setDebug(1);
				return $this->apiInstance->cpanelAPI2Query($userName, 'AddonDomain', 'listaddondomains', $listParam);
			}
		}
		return false;
	}

	/**
	 * This function deletes an addon domain and its subdomain.
	 * @param string [cPanel_user_name] 
	 * @param Array [add_on_detail] 
	 * @param string [add_on_detail[domain]] 
	 * @param string [add_on_detail[subdomain]](domainKey)
	 * Note:  domainKey = subdomain_maindomain of account
	 * @return response 
	 * @link https://documentation.cpanel.net/display/DD/cPanel+API+2+Functions+-+AddonDomain%3A%3Aaddaddondomain
	 */
	public function removeAddonDomain($param = [])
	{
		$this->set_apiInstance('whm');
		if (!empty($param))
		{
			$userName = isset($param['cPanel_user_name']) ? $param['cPanel_user_name'] : '';
			$domainDetails = isset($param['add_on_detail']) ? $param['add_on_detail'] : '';
			if (!empty($userName) && !empty($domainDetails))
			{
				// $this->apiInstance->setDebug(1);
				return $this->apiInstance->cpanelAPI2Query($userName, 'AddonDomain', 'deladdondomain', $domainDetails);
			}
		}
		return false;
	}

	/**
	 * This function creates an alias (parks a domain on another domain).
	 * @param string userName [cPanel_user_name] 
	 * @param string domain, The domain name to park. 
	 * @param string topDomain, The subdomain on which to park the domain parameter's domain.
	 * @return response 
	 * @link https://documentation.cpanel.net/display/DD/cPanel+API+2+Functions+-+Park%3A%3Apark
	 */
	public function parkDomain($userName, $domain, $topDomain)
	{
		$this->set_apiInstance('whm');
		if ($this->apiInstance)
		{
			if (!empty($domain) && !empty($topDomain))
			{
				$param = [
					'domain' => $domain,
					'topdomain' => $topDomain
 				];
				return $this->apiInstance->cpanelAPI2Query($userName, 'Park', 'park', $param);
			}
		}
		return false;
	}

	/**
	 * This function removes a parked domain (alias) or an addon domain.
	 * @param string userName [cPanel_user_name] 
	 * @param string domain, The parked domain. 
	 * @param string topDomain, The addon domain or subdomain's parent domain.
	 * @return response 
	 * @link https://documentation.cpanel.net/display/DD/cPanel+API+2+Functions+-+Park%3A%3Aunpark
	 */
	public function unParkDomain($userName, $domain, $subdomain)
	{
		$this->set_apiInstance('whm');
		if ($this->apiInstance)
		{
			if (!empty($domain) && !empty($subdomain))
			{
				$param = [
					'domain' => $domain,
					'subdomain' => $subdomain
 				];
				return $this->apiInstance->cpanelAPI2Query($userName, 'Park', 'unpark', $param);
			}
		}
		return false;
	}


	/**
    * Get the version of cPanel running on the server
    *
    * This function will return the version of cPanel/WHM running on the remote system
    *
    * @link https://documentation.cpanel.net/display/DD/WHM+API+1+Functions+-+version XML API Call documentation
    */
	public function getWHMVersion()
	{
		$this->set_apiInstance('whm');
		if ($this->apiInstance)
		{
			return $this->apiInstance->version();
		}
	}


	/**
    * List IPs
    *
    * This should return a list of IPs on a server
    * @return mixed
	documentation
    */
	public function listsIps()
	{
		$this->set_apiInstance('whm');
		if ($this->apiInstance)
		{
			return $this->apiInstance->listips();
		}
	}


	/**
    * Suspend or UnSuspend a User's Account
    *Set $suspend = 1 for suspend , and 0 for unsuspend
    * This function will suspend the specified cPanel users account.
    * The $reason parameter is optional, but can contain a string of any length
    *
    * @param string $username The username to suspend
    * @param string $reason The reason for the suspension
    * @return mixed
    * @link https://documentation.cpanel.net/display/DD/WHM+API+1+Functions+-+suspendacct XML API Call documentation
    * @link https://documentation.cpanel.net/display/DD/WHM+API+1+Functions+-+unsuspendacct
    */
	public function suspendAccount($param = [], $suspend = 1)
	{
		$this->set_apiInstance('whm');
		try
		{
			$userName = isset($param['user_name']) ? $param['user_name'] : '';
			$reason = isset($param['reason']) ? $param['reason'] : '';
			if (!empty($userName) && !empty($reason))
			{
				if ($suspend == 1)
				{
					return $this->apiInstance->suspendacct($param['user_name'], $param['reason']);
				}
				elseif ($suspend == 0)
				{
					return $this->apiInstance->unsuspendacct($param['user_name']);
				}
			}
		}
		catch (Exception $ex)
		{
	        return response()->json([
                "success" => false,
                "error" => $ex->getMessage(),
                "message" => $ex->getMessage()
            ]);
		}
		return false;
	}


	/**
    * Return a summary of the account's information
    *
    * This call will return a brief report of information about an account, such as:
    *	- Disk Limit
    *	- Disk Used
    *	- Domain
    *	- Account Email
    *	- Theme
    * 	- Start Data
    *
    * Please see the XML API Call documentation for more information on what is returned by this call
    *
    * @param string $username The username to retrieve a summary of
    * @return mixed
    * @link https://documentation.cpanel.net/display/DD/WHM+API+1+Functions+-+accountsummary XML API Call documenation
    */
	public function accountSummary($username = "")
	{
		$this->set_apiInstance('whm');
		try
		{
			return $this->apiInstance->accountsummary($username);
		}
		catch (Exception $ex)
		{
			return false;
		}
	}


	/**
	 * This function adds a redirect to a domain.
	 * 
	 * @param string $domain The domain from which to redirect.
	 * @param string $redirect The URL to which to redirect.
	 * @param string $type Whether the redirect is temporary.
	 * @param int $redirect_wildcard Whether to redirect all files within a directory to the same filename within the destination directory.
	 * @param int $redirect_www Whether to redirect domains with or without www.
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+Mime::add_redirect
	 */
	public function addDomainRedirect($domain, $redirect, $type = 'permanent', $redirect_wildcard = 1, $redirect_www = 0)
	{
		if ($this->apiInstance) {

			$this->apiInstance->setScope('Mime');
			return $this->apiInstance->add_redirect([
					'domain' => $domain, 
					'redirect' => $redirect, 
					'type' => $type, 
					'redirect_wildcard' => $redirect_wildcard,
					'redirect_www' => $redirect_www
			]);
		}
	}


	/**
	 * This function removes a redirect from a domain.
	 * 
	 * @param string $domain The domain name.
	 * @param string $docroot An absolute file path.
	 * @param string $args An argument string that contains the arguments of a Redirect or RedirectMatchdirectives. for ex. google.com
	 * @param string $src The specific page that redirects visitors.
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+Mime%3A%3Adelete_redirect
	 */
	public function removeDomainRedirect($domain, $docroot, $args = '', $src = '')
	{
		if ($this->apiInstance) {
			$this->apiInstance->setScope('Mime');
			return $this->apiInstance->delete_redirect([
					'domain' => $domain,
					'src' => $src,
					'docroot' => $docroot,
			]);
		}
	}


	/**
	 * This function creates an autoresponder for an email account.
	 * 
	 * @param string $email A valid email account name on the server.
	 * @param string $userName The contents of the autoresponder message's From: field.
	 * @param string $subject The contents of the autoresponder message's Subject: field.
	 * @param string $body The contents of the autoresponder message's Body section.
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+Email%3A%3Aadd_auto_responder
	 */
	public function setAutoResponder($email, $userName, $subject, $body, $interval = 0, $isHtml = 1)
	{
		if ($this->apiInstance)
		{
			$this->apiInstance->setScope('Email');
			$autoResponderParam = [
				'email' => $email,
				'from' => $userName,
				'subject' => $subject,
				'body' => $body,
				'domain' => substr(strrchr($email, "@"), 1),
				'is_html' => ($isHtml) ? TRUE : FALSE,
				'interval' => $interval,
				'start' => 0,
				'stop' => 0
			];
			return $this->apiInstance->add_auto_responder($autoResponderParam);
		}
		else
		{
			throw new EmptyInstanceException();
		}
	}

	
	/**
	 * This function deletes an autoresponder.
	 * 
	 * @param string $email A valid email account name on the server.
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+Email%3A%3Adelete_auto_responder
	 */	
	public function deleteAutoResponder($email)
	{
		if ($this->apiInstance) {
			$this->apiInstance->setScope('Email');
			return $this->apiInstance->delete_auto_responder(['email' => $email]);
		}
	}


	/**
	 * This function creates an email forwarder.
	 * 
	 * @param string $domain A  valid domain on the account.
	 * @param string $forwardingEmail The email address to forward.
	 * @param string $fwdopt The method to use to handle the email address's mail.
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+Email%3A%3Aadd_forwarder
	 */	
	public function setForwarder($domain, $forwardingEmail, $forwardToEmail)
	{
		if ($this->apiInstance) {		
			$this->apiInstance->setScope('Email');
			$forwarderParam = [
				'domain' => $domain,
				'email' => $forwardingEmail,
				'fwdopt' => 'fwd',
				'fwdemail' => $forwardToEmail,
			];
			return $this->apiInstance->add_forwarder($forwarderParam);
		}
		else
		{
			throw new EmptyInstanceException();
		}
	}


	/**
	 * This function deletes an email forwarder.
	 * 
	 * @param string $forwardingEmail The forwarder's email address.
	 * @param string $forwardToEmail The forwarder's destination.
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+Email%3A%3Adelete_forwarder
	 */	
	public function deleteForwarder($forwardingEmail, $forwardToEmail)
	{
		if ($this->apiInstance) {
			$this->apiInstance->setScope('Email');
			$deleteForwarderParam = [
				'address' => $forwardingEmail,
				'forwarder' => $forwardToEmail,
			];
			return $this->apiInstance->delete_forwarder($deleteForwarderParam);
		}
		else
		{
			throw new EmptyInstanceException();
		}
	}


	/**
	 * This function creates an email address.
	 * 
	 * @param string $email The email account username or address.
	 * @param string $password
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+Email%3A%3Aadd_pop
	 */	
	public function createEmail($email, $password, $domain, $quota = 500)
	{
		if ($this->apiInstance) {
			$this->apiInstance->setScope('Email');
			$createEmailAccParam = [
				'email' => $email,
				'password' => $password,
				'domain' => $domain,
				'quota' => $quota
			];
			return $this->apiInstance->add_pop($createEmailAccParam);
		}
		else
		{
			throw new EmptyInstanceException();
		}
	}


	/**
	 * This function changes an email account's password.
	 * 
	 * @param array $param should contain email and password 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+Email%3A%3Apasswd_pop
	 */	
	public function resetEmailPassword($param = [])
	{
		if ($this->apiInstance) {
			$this->apiInstance->setScope('Email');
			return $this->apiInstance->passwd_pop($param);
		}
		else
		{
			throw new EmptyInstanceException();
		}
	}


	/**
	 * This function enables Domain Keys Identified Mail (DKIM) records on the DNS server for one or more domains.
	 * @param string $domain The domain for which to enable DKIM records on the DNS server.
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+EmailAuth%3A%3Aenable_dkim
	 */	
	public function enableDKIM($domain)
	{
		if ($this->apiInstance) {
			$this->apiInstance->setScope('EmailAuth');
	        $param                    = [
	            'domain' => $domain
	        ];
	        return $this->apiInstance->enable_dkim($param);
	    }
    }


    /**
	 * This function removes the Domain Keys Identified Mail (DKIM) records on the DNS server for one or more domains.
	 * @param string $domain The domain for which to remove DKIM records on the DNS server.
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+EmailAuth%3A%3Adisable_dkim
	 */	
    public function disableDKIM($domain)
	{
		if ($this->apiInstance) {
			$this->apiInstance->setScope('EmailAuth');
	        $param                    = [
	            'domain' => $domain
	        ];
	        return $this->apiInstance->disable_dkim($param);
	    }
    }


    /**
	 * This function retrieves and checks the Domain Keys Identified Mail (DKIM) records for one or more domains.
	 *
	 * @param string $domain The domain for which to check the DKIM records.
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+EmailAuth%3A%3Avalidate_current_dkims
	 */	
    public function getDKIM($domain) 
    {
		if ($this->apiInstance) {
			$this->apiInstance->setScope('EmailAuth');
	        $param                    = [
	            'domain' => $domain
	        ];
	        return $this->apiInstance->validate_current_dkims($param);
	    }
	}


	/**
	 * This function generates a self-signed SSL certificate.
	 * array $param must contain
	 * @param string domains The domain(s) for which to generate the certificate.
	 * @param string countryName The two-letter country code.
	 * @param string stateOrProvinceName The two-letter state or locality abbreviation.
	 * @param string localityName The certificate's city name.
	 * @param string organizationName The certificate's organization.
	 * @param string key_id The key's ID.
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+SSL%3A%3Agenerate_cert
	 */
	public function generateSSLCrt($param = [])
	{
		if ($this->apiInstance)
		{
			if (empty($param['domains']) || empty($param['countryName']) || empty($param['key_id']))
			{
				throw new InvalidDataException();
			}
			$this->apiInstance->setScope('SSL');
			return $this->apiInstance->generate_cert($param);
		}
		
	}


	/**
	 * This function generates a certificate signing request (CSR).
	 * array $param must contain
	 * @param string domains The domain(s) for which to generate the certificate.
	 * @param string countryName The two-letter country code.
	 * @param string stateOrProvinceName The two-letter state or locality abbreviation.
	 * @param string localityName The certificate's city name.
	 * @param string organizationName The certificate's organization.
	 * @param string key_id The key's ID.
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+SSL%3A%3Agenerate_csr
	*/
	public function generateSSLCsr($param = [])
	{
		if ($this->apiInstance)
		{
			if (empty($param['domains']) || empty($param['countryName']) || empty($param['key_id']))
			{
				throw new InvalidDataException();
			}
			$this->apiInstance->setScope('SSL');
			return $this->apiInstance->generate_csr($param);
		}
		
	}


	/**
	 * This function generates a private key.
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+SSL%3A%3Agenerate_key
	*/
	public function generateSSLKey($param = [])
	{
		if ($this->apiInstance)
		{
			$this->apiInstance->setScope('SSL');
			return $this->apiInstance->generate_key($param);
		}
		
	}


	/**
	 * This function retrieves a certificate's CA bundle and hostname.
	 * array $param must contain
	 * @param string cert The certificate's text
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+SSL%3A%3Aget_cabundle
	*/
	public function generateSSLCab($param = [])
	{
		if ($this->apiInstance)
		{
			if (empty($param['cert']))
			{
				throw new InvalidDataException();
			}
			$this->apiInstance->setScope('SSL');
			return $this->apiInstance->get_cabundle($param);
		}
		
	}


	/**
	 * This function lists SSL-related items on a domain.
	 * array $param must contain
	 * @param string $item The SSL item type or types, This parameter defaults to key.
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+SSL%3A%3Alist_ssl_items
	*/
	public function listSSLItems($domain, $item)
	{
		if ($this->apiInstance)
		{
			$param = [
				'domains'  => $domain,
				'items'    => $item
			];

			$this->apiInstance->setScope('SSL');
			return $this->apiInstance->list_ssl_items($param);
		}
		
	}


	/**
	 * This function installs an SSL certificate.
	 * array $param must contain
	 * @param string domain The domain name.
	 * @param string cert The certificate to install.
	 * @param string key The certificate's key.
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+SSL%3A%3Ainstall_ssl
	*/
	public function installSSL($param = [])
	{
		if ($this->apiInstance)
		{
			if (empty($param['domain']) || empty($param['cert']) || empty($param['key']))
			{
				throw new InvalidDataException();
			}

			$this->apiInstance->setScope('SSL');
			return $this->apiInstance->install_ssl($param);
		}
		
	}


	/**
	 * This function lists Mail Exchanger (MX) records.
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+Email%3A%3Alist_mxs
	*/
	public function getDomainMX($domain) 
    {
		if ($this->apiInstance) {
			$this->apiInstance->setScope('Email');
	        $param                    = [
	            'domain' => $domain
	        ];
	        return $this->apiInstance->list_mxs($param);
	    }
	}


	/**
	 * This function modifies a Mail Exchanger (MX) record
	 * array $param must contain
	 * @param string domain The mail exchanger's domain.
	 * @param string exchanger The mail exchanger's name.
	 * @param string oldexchanger The mail exchanger's current name.
	 * @param string priority The mail exchanger's new priority value.
	 * 
	 * @link https://documentation.cpanel.net/display/DD/UAPI+Functions+-+Email%3A%3Achange_mx
	*/
	public function updateDomainMX($param = []) 
    {
		if ($this->apiInstance) {
			if (empty($param['domain']) || empty($param['exchanger']) || empty($param['oldexchanger']))
			{
				throw new InvalidDataException();
			}
			$this->apiInstance->setScope('Email');
	        return $this->apiInstance->change_mx($change_mx);
	    }
	}


	/**
	 * This function deletes a cPanel or WHM account.
	 * Note - You cannot recover deleted accounts. 
	 * @link https://documentation.cpanel.net/display/DD/WHM+API+1+Functions+-+removeacct
	*/
	public function deleteWhmAccount($username, $keepdns = false)
	{
		$this->set_apiInstance('whm');
		if ($this->apiInstance) {
			$this->apiInstance->setDebug(1);
			return $this->apiInstance->removeAcct($username, $keepdns);
		}
	}

}