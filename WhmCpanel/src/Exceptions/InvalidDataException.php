<?php

namespace Unify\CpanelWhm\Exceptions;

use Unify\CpanelWhm\Exceptions\Exceptions;

class InvalidDataException extends Exception
{
    /**
     * Create a new exception instance.
     *
     * @return void
     */
    public function __construct($message="Data is not valid", $code=401) {
        parent::__construct($message, $code);
    }
}