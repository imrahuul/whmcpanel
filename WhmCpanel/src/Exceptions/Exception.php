<?php

namespace Unify\CpanelWhm\Exceptions;

use Exception as Ex;

class Exception extends Ex
{
    public function __construct($message, $code=0) {
        parent::__construct($message, $code);
    }
}