<?php

namespace Unify\CpanelWhm;

use Exception;

class WhmCpanelApi {

    // should debugging statements be printed?
    private $debug			= true; //false;

    // The host to connect to
    private $host				=	'127.0.0.1';

    // the port to connect to
    private $port				=	'2087';

    // should be the literal strings http or https
    private $protocol		=	'https';

    // output that should be given by the xml-api
    private $output		=	'json';

    // literal strings hash or password
    private $auth_type 	= null;

    //  the actual password or hash
    private $auth 			= null;

    // username to authenticate as
    private $user				= null;

    // The HTTP Client to use

    private $http_client		= 'curl';

    protected $url;

    public function __construct($host = null, $user = null, $password = null )
    {
    	if ( ( $user != null ) ) {
            $this->user = $user;
        }

        if ($password != null) {
            $this->set_password($password);
        }

        if ($host != null) {
				$this->host = $host;
        } else {
            throw new Exception("No host defined");
        }
	}


    /**
    * Set the password to be autenticated with
    *
    * This will set the password to be authenticated with, the auth_type will be automatically adjusted
    * when this function is used
    *
    * @param string $pass the password to authenticate with
    */
	public function set_password( $pass )
    {
        $this->auth_type = 'pass';
        $this->auth = $pass;
    }


    /**
    * Return what format calls with be returned in
    *
    * This function will return the currently set output format
    * @return string
    */
    public function get_output()
    {
        return $this->output;
    }


    /**
    * Turn on debug mode
    *
    * Enabling this option will cause this script to print debug information such as
    * the queries made, the response XML/JSON and other such pertinent information.
    * Calling this function without any parameters will enable debug mode.
    *
    * @param bool $debug turn on or off debug mode
    */
    public function setDebug( $debug = 1 )
    {
        $this->debug = $debug;
    }


    /**
    * Return the Test Output 
    *
    * @return Array
    */
    public function get_testresult()
    {
        $result = [
            'output' => $this->output,
            'auth'   =>  $this->auth_type,
            'user'   => $this->user,
            'pass'   => $this->auth,
            'host'   => $this->host
        ];
        return $result;
    }


    /**
    *   Query Functions
    *   --
    *   This is where the actual calling of the XML-API, building API1 & API2 calls happens
    */

    /**
    * Perform an XML-API Query
    *
    * This function will perform an XML-API Query and return the specified output format of the call being made
    *
    * @param string $function The XML-API call to execute
    * @param array $vars An associative array of the parameters to be passed to the XML-API Calls
    * @return mixed
    */
    public function whmCpanelQuery( $function, $vars = [] )
    {
        // Check to make sure all the data needed to perform the query is in place
        if (!$function) {
            throw new Exception('whmCpanelQuery() requires a function to be passed to it');
        }

        if ($this->user == null) {
            throw new Exception('no user has been set');
        }

        if ($this->auth ==null) {
            throw new Exception('no authentication information has been set');
        }

        // Build the query:

        $query_type = '/xml-api/';

        if ($this->output == 'json') {
            $query_type = '/json-api/';
        }

        $args = http_build_query($vars, '', '&');
        $this->url =  $this->protocol . '://' . $this->host . ':' . $this->port . $query_type . $function;

        if ($this->debug) {
            error_log('URL: ' . $this->url);
            error_log('DATA: ' . $args);
        }

        // Set the $auth string

        $authstr = NULL;
        if ($this->auth_type == 'hash') {
            $authstr = 'Authorization: WHM ' . $this->user . ':' . $this->auth . "\r\n";
        } elseif ($this->auth_type == 'pass') {
            $authstr = 'Authorization: Basic ' . base64_encode($this->user .':'. $this->auth) . "\r\n";
        } else {
            throw new Exception('invalid auth_type set');
        }

        if ($this->debug) {
            error_log("Authentication Header: " . $authstr ."\n");
        }

        // Perform the query (or pass the info to the functions that actually do perform the query)

        $response = NULL;
        if ($this->http_client == 'curl') {
            $response = $this->curlRequest($this->url, $args, $authstr);
        }

        /*
        *	Post-Query Block
        * Handle response, return proper data types, debug, etc
        */

        // print out the response if debug mode is enabled.
        if ($this->debug) {
            error_log("RESPONSE:\n " . $response);
        }

        // The only time a response should contain <html> is in the case of authentication error
        // cPanel 11.25 fixes this issue, but if <html> is in the response, we'll error out.

        if (stristr($response, '<html>') == true) {
            if (stristr($response, 'Login Attempt Failed') == true) {
                error_log("Login Attempt Failed");

                return;
            }
            if (stristr($response, 'action="/login/"') == true) {
                error_log("Authentication Error");

                return;
            }

            return;
        }

        return $response;
    }


    /**
    * Call an API2 Function
    *
    * This function allows you to call an API2 function,Although cPanel UAPI is the modern API for cPanel and should be used in preference over
    * API1 , API2 when possible
    *
    * @param string $user The username of the account to perform API2 actions on
    * @param string $module The module of the API2 call to use
    * @param string $function The function of the API2 call
    * @param array $args An associative array containing the arguments for the API2 call
    * @return mixed
    * @link https://documentation.cpanel.net/display/DD/Guide+to+cPanel+API+2
    */
    public function cpanelAPI2Query($user, $module, $function, $args = [])
    {
        if (!isset($user) || !isset($module) || !isset($function) ) {
            return false;
        }
        if (!is_array($args)) {
            throw new Exception("api2_query requires that an array is passed to it as the 4th parameter");

            return false;
        }

        $cpuser = 'cpanel_xmlapi_user';
        $module_type = 'cpanel_xmlapi_module';
        $func_type = 'cpanel_xmlapi_func';
        $api_type = 'cpanel_xmlapi_apiversion';

        if ( $this->get_output() == 'json' ) {
            $cpuser = 'cpanel_jsonapi_user';
            $module_type = 'cpanel_jsonapi_module';
            $func_type = 'cpanel_jsonapi_func';
            $api_type = 'cpanel_jsonapi_apiversion';
        }

        $args[$cpuser] = $user;
        $args[$module_type] = $module;
        $args[$func_type] = $function;
        $args[$api_type] = '2';

        return $this->whmCpanelQuery('cpanel', $args);
    }

    private function curlRequest( $url, $postdata, $authstr )
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        // Return contents of transfer on curl_exec
         curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // Allow self-signed certs
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // Set the URL
        curl_setopt($curl, CURLOPT_URL, $url);
        // Increase buffer size to avoid "funny output" exception
        curl_setopt($curl, CURLOPT_BUFFERSIZE, 131072);

        // Pass authentication header
        $header[0] =$authstr .
            "Content-Type: application/x-www-form-urlencoded\r\n" .
            "Content-Length: " . strlen($postdata) . "\r\n" . "\r\n" . $postdata;

        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

        curl_setopt($curl, CURLOPT_POST, 1);

        $result = curl_exec($curl);
        if ($result == false) {
            throw new Exception("curl_exec threw error \"" . curl_error($curl) . "\" for " . $url . "?" . $postdata );
        }
        curl_close($curl);

        return $result;
    }


    /**
    * Return The Last request made using this package
    *
    * @return
    */
    public function fetchLastRequest()
    {
        return $this->url;
    }

    public function createacct($acctconf)
    {
        if (!is_array($acctconf)) {
            return false;
        }
        if (!isset($acctconf['username']) || !isset($acctconf['password']) || !isset($acctconf['domain'])) {
            return false;
        }

        return $this->whmCpanelQuery('createacct', $acctconf);
    }


    public function accountsummary($username)
    {
        if (!isset($username)) {
            return false;
        }

        return $this->whmCpanelQuery('accountsummary', ['user' => $username]);
    }


    /**
    * Suspend a User's Account
    *
    * This function will suspend the specified cPanel users account.
    * The $reason parameter is optional, but can contain a string of any length
    *
    * @param string $username The username to suspend
    * @param string $reason The reason for the suspension
    * @return mixed
    */
    public function suspendacct($username, $reason = null)
    {
        if (!isset($username)) {
            return false;
        }
        if ($reason) {
            return $this->whmCpanelQuery('suspendacct', ['user' => $username, 'reason' => $reason]);
        }
        return $this->whmCpanelQuery('suspendacct', ['user' => $username]);
    }

    public function unsuspendacct($username)
    {
        if (!isset($username)) {
            return false;
        }

        return $this->whmCpanelQuery('unsuspendacct', ['user' => $username]);
    }

    public function version()
    {
        return $this->whmCpanelQuery('version');
    }

    public function listips()
    {
        return $this->whmCpanelQuery('listips');
    }

    public function removeAcct($username, $keepdns = false)
    {
        if (!isset($username)) {
           return false;
        }
        if ($keepdns) {
            return $this->whmCpanelQuery('removeacct', ['user' => $username, 'keepdns' => '1']);
        }
        return $this->whmCpanelQuery('removeacct', ['user' => $username]);
    }
}
